<?php

/*
Google Authenticator / mod_authn_otp configuration generator by Terry Carmen (terry@cnysupport.com).

Many portions written/copyright by others as noted below
2016 Thomas Zink, modifications to work with HOTP / TOTP and provided secret; general cleanup
Copyright 2012, CNY Support, LLC, Licenced under GPL v3
*/

define("RANDOM_DEVICE", '/dev/urandom'); // fast, mostly random
#define("RANDOM_DEVICE", '/dev/random'); // slow, potential block, highly random

include './phpqrcode.php';
include './lib.php';

// check URL
if (!isset($_GET["site"]) || !isset($_GET["user"]) || strlen($_GET["site"]) == 0 || strlen($_GET["user"]) == 0) {
	echo "<h2>usage: ga.php?site=your site&amp;user=your user&amp;secret=&amp;algo=otpalgorithm</h2>";
	echo "<h3>Valid characters are: 0-9, A-Z, a-z, - and space</h3>";
	echo "<h3>Example:</h3>";
	echo "<h3>ga.php?site=example.com&user=dummy&secret=&algo=TOTP</h3>";
	exit (0);
}

// sanitize and convert the input
// do not use built-in base_convert! Does not support base32 encoding

$ga = new PHPGangsta_GoogleAuthenticator();
$base = new Base();

// secret for Google Authenticator must be base 32
if (isset($_GET["secret"]) && strlen($_GET["secret"]) > 0) {
	// if set, secret comes in hex
	$secret_b16 = $_GET["secret"];	
	//$secret_b32 = base_base2base($secret_b16, 16, 32);
	$secret_b32 = $base->base2base($secret_b16, 16, 32);
} else {
	// no secret provided, generate one in base 32
	$secret_b16 = "";
	while ((strlen($secret_b16) % 2) != 0 || strlen($secret_b16) == 0) {
		$secret_b32 = $ga->createSecret(32);
		//$secret_b16 = base_base2base($secret_b32, 32, 16);
		$secret_b16 = $base->base2base($secret_b32, 32, 16);
	}
}

$site = preg_replace( "/[^0-9 _a-zA-Z\.-]/", '', substr($_GET["site"],0,64));
$user = preg_replace( "/[^0-9 _a-zA-Z\.-]/", '', substr($_GET["user"],0,64));
$otp = preg_replace( "/[^0-9 _a-zA-Z\.-]/", '', strtolower(substr($_GET["algo"],0,64)));

if ($otp=="totp") {
	$tokentype = "HOTP/T30";
} else if ($otp=="hotp") {
	$tokentype = "HOTP/E";
}

// Output the web page . . .
?>
<!DOCTYPE html>

<html lang="en-US">
<head>
<title>gauthnqr</title>
</head>
<body>
<h2>Google Auth / FreeOTP / mod-authn-otp Account Generator</h2>
<table>
<tr></tr>
<tr><td>Site:</td><td> <?php echo $site ?></td></tr>
<tr><td>User:</td><td> <?php echo $user ?></td></tr>
<tr><td>Key (b32, google auth):</td><td> <?php echo $secret_b32 ?></td></tr>
<tr><td>Key (hex, mod_authn_otp):</td><td> <?php echo $secret_b16 ?></td></tr>
<tr><td>Uri:</td><td> otpauth://<?php echo $otp ?>/<?php echo $site ?>?secret=<?php echo $secret_b32 ?></td></tr>
<tr><td></br></td></tr>

<?php
if ($otp=="hotp") {
	echo "<tr><td>Yubikey slot 1:</td><td> <pre style=font-size:larger;>ykpersonalize -1 -ooath-hotp -ooath-imf=0 -ofixed= -oappend-cr -a$secret_b16 </pre></td></tr>";
	echo "<tr><td>Yubikey slot 2:</td><td> <pre style=font-size:larger;>ykpersonalize -2 -ooath-hotp -ooath-imf=0 -ofixed= -oappend-cr -a$secret_b16 </pre></td></tr>";
}
?>

<tr><td>otp_users config line:</td><td> <pre style=font-size:larger;><?php echo $tokentype ?> <?php echo $user ?>  -  <?php echo $secret_b16 ?></pre></td></tr>

</table>
<table>
<TR><TD>QR Code for gAuthenticator or FreeOTP:</TD></TR>
<tr><td>
<iframe style="border:none;" height="252" width="252" src="qr.php?url=otpauth://<?php echo $otp ?>/<?php echo $site ?>%3Fsecret%3D<?php echo $secret_b32 ?>"></iframe>
</td></tr>
<TR><TD>That's it!</TD></TR>
</table>
</body>
</html>

